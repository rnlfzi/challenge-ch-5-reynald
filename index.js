const express = require("express");
const AuthRoute = require("./routes/AuthRoute.js");
const WebRoute = require("./routes/WebRoute.js");

const app = express();
const port = 3330;

app.set('view engine', 'ejs');

app.use(express.static('./public'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(WebRoute);
app.use(AuthRoute);

app.listen(port, () => console.log(`server is running in http://localhost:${port}`));