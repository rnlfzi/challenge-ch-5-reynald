const LoginPage = (req, res) => {
    res.render('Login');
}

const Dashboard = (req, res) => {
    const userLogin = require("../login.json");

    const username = userLogin.find((row) => row.username);

    res.render('home', username)
}

const LandingPage = (req, res) => {
    res.render('index');
}

const GamePage = (req, res) => {
    res.render('index-game');
}

const NotFound = (req, res) => {
    res.render('404');
}

module.exports = {LoginPage, Dashboard , LandingPage, GamePage, NotFound}