const users = require("../users.json");
const fs = require("fs");

const login = (req, res) => {
    const user = users.find((row) => row.email == req.body.email);

    const payload = [{ id: user.id, username: user.username, email: user.email }]

    fs.writeFileSync('./login.json', JSON.stringify(payload));

    res.redirect(`/dashboard`)
}

const Logout = (req, res) => {
    const userLogout = [];

    fs.writeFileSync('./login.json', JSON.stringify(userLogout));

    res.redirect('/')
}

module.exports = {login, Logout};

