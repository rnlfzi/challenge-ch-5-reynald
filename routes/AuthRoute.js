const express = require("express");
const {login, Logout} = require("../controller/Auth.js");
const {validation} = require("../middleware/validation.js");

const router = express.Router();

router.post('/', validation, login);
router.post('/logout', Logout);

module.exports = router;