const express = require("express");
const {LoginPage, LandingPage, GamePage, NotFound, Dashboard} = require("../controller/WebControl.js");

const router = express.Router();

router.get('/', LoginPage);
router.get('/dashboard', Dashboard)
router.get('/landing', LandingPage);
router.get('/games', GamePage);
router.get('*', NotFound);

module.exports = router;

