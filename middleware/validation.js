const users = require("../users.json");

const validation = (req, res, next) => {
    const user = users.find((row) => row.email == req.body.email);
    const match = user.password === req.body.password;

    if(!user) {
        res.status(404).json({ msg: 'Email is not registered'});

    }else if(!match) {
        res.status(400).json({ msg: 'Wrong password'});

    } else {
        next();
    }
}

module.exports = {validation};